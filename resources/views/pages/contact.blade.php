@extends('layouts.master')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col mapContainer">
			<div id="map"></div>
		</div>
	</div>
</div>
<div class="container mt-5">
	<div class="row">
		<div class="col-md-6 col-12 iletisim">
			<h4>Bize Ulaşın</h4>
			<h5 class="mt-4">Fabrika</h5>
			<h6 class="mt-4">Mekan A.Ş. Mekan Banyo Ürünleri Yapı Gereçleri Sa. Tic. A.Ş.</h6>
			<p>Dereler Kötü Mah. Beton Santrali Yolu 228 Canik / Samsun</p>
			<p>Telefon: +90 (362) 228 77 44</p>
			<p>E-posta: info@carettabanyo.com</p>
			<h5 class="mt-4">Mağaza</h5>
			<p>Hançerli Mahallesi No:61/B İlkadım/Samsun</p>
			<p>+90 (362) 431 64 00</p>
		</div>
		<div class="col-md-6 col-12 iletisim mb-5">
			<h4>Bize Yazın</h4>
			{!! Form::open(['route'=>'contact.send_mail', 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
				<div class="form-group mt-4">
          {{ Form::label('name','Adınız Soyadınız') }}
					{{ Form::text('name',null,['class'=>'form-control','required'=>'','data-parsley-error-message'=>'Adınız Soyadınız zorunlu alan.'])}}
				</div>
				<div class="form-group">
          {{ Form::label('email','E-posta') }}
					{{ Form::email('email',null,['class'=>'form-control','required'=>'','data-parsley-error-message'=>'E-posta zorunlu alan.'])}}
				</div>
				<div class="form-group">
          {{ Form::label('phone','Telefon') }}
					{{ Form::text('phone',null,['class'=>'form-control'])}}
				</div>
				<div class="form-group">
          {{ Form::label('subject','Konu') }}
					{{ Form::text('subject',null,['class'=>'form-control'])}}
				</div>
				<div class="form-group">
          {{ Form::label('user_message','Mesajınız') }}
					{{ Form::textarea('user_message',null,['class'=>'form-control'])}}
				</div>
        <div class="float-right">
				{{ Form::submit('Gönder',['class'=>'btn btn-sea_color'])}}
        </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


@endsection

@section('custom_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgfalBHhT1ChxYdmsdlggWRCwqTgBrftM&callback=initMap" async defer></script>
<script>
    function initMap() {

      var orta = {lat: 41.260205, lng: 36.350088};
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom:14,
        scrollwheel: false,
        center: orta,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      /* ------------------------------MARKER 1----------------------------------------------- */

      var marker = new google.maps.Marker({
        position: {lat: 41.249205, lng: 36.308088},
        map: map
      });

      /* ----------------------------------------------------------------------------- */

    }
  $(document).ready(function() {

    $("#map").addClass("scrolloff");
				$("#map").addClass("scrolloff");
				$(".mapContainer").on("mousedown",function(){
					$("#map").removeClass("scrolloff");
				});
				$("#map").mouseleave(function(){
					$("#map").addClass("scrolloff")
				});

  })
</script>
@endsection
