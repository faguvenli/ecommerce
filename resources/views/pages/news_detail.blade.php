@extends('layouts.iframe_main')

@section('content')

<div class="content">
  <div class="container-fluid">
    @if($haber)
    <div class="row">
      <div class="col-12 col-md-5">
        <img src="{{ asset('images/news/'.$haber->haber_gorsel) }}" class="img-fluid w-100">
      </div>
      <div class="col-12 col-md-7">
        <div class="row" style="height:100%">
          <div class="col-12">
            <div class="bg-acik-gri detail_column" style="height:100%">
              <h2 class="text-sea_color mt-1"><strong>{{ $haber->baslik }}</strong></h2>
              <div class="mt-4">{!! $haber->icerik !!}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
</div>


@endsection

@section('custom_scripts')
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
