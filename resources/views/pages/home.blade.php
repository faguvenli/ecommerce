@extends('layouts.master')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col p-0">
      <div class="master-slider ms-skin-default" id="masterslider">

						<!-- new slide -->
						<div class="ms-slide">
							<img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="{{ asset('images/slider1.jpg') }}"/>
							<!-- slide image layer -->
              <!-- slide text layer -->
              <div class="ms-layer ms-caption home"
                   data-offset-x      = "0"
                   data-offset-y      = "0"
                   data-position      = "normal"
                   data-origin        = "tl"
                   data-type          = "text"
                   data-effect        = "bottom(90)"
                   data-duration      = "800"
                   data-ease          = "easeOutQuart"
                   data-resize        = "false"
                   data-fixed         = "false"
                   data-widthlimit    = "600"

              >
               <h2>Hayallerinizdeki<br>banyolar</h2>
               <p>Banyo dolapları, duş ve küvet kabinleri, akrilik küvet ve duş teknelerinde herkese uyacak yüzlerce seçenek...</p>
               <p>Size düşen hayalinizdeki banyoyu seçmek.</p>

              </div>
						</div>
						<!-- end slide -->


					</div> <!-- master slider end -->
    </div>
  </div>
</div>

<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-10 col-xl-7">
      <h1 class="text-center text-sea_color">Ürünlerimiz</h1>
      <p class="text-center">Banyo Mobilyaları, Duş Küvet Kabinleri, Banyo ve WC Menfezleri, Tekve ve Küvetler, Köşe Profilleri, Banyo Aksesuarları, Armatürler, Seramik Çeşitleri, Granit, Fayans ve Seramik Yapıştırıcıları.. Ürünlerimizin bütün serileri hizmetinizdedir.</p>
    </div>
  </div>
  <div class="row mt-4 kutular">
    <?php
      $renkler = ['kahverengi_bg','mavi_bg','gri_bg','kirmizi_bg'];
      $i = 0;
    ?>
    @foreach($main_menu as $menu)
    <div class="col-12 col-sm-6 col-lg-3 mb-3">
      <a href="{{ url('/category',['id'=>$menu->id,'name'=>str_slug($menu->name)]) }}">
        <div><img src="{{ asset('images/categories/medium/'.$menu->category_image) }}" class="img-fluid w-100"></div>
        <div class="{{ $renkler[$i] }} p-2 mt-1"><h5 class="m-0">{{ $menu->name }}</h5></div>
      </a>
    </div>
    <?php $i++; ?>
    @endforeach
  </div>
  <div class="row mt-5">
    <div class="col-12 col-lg-6"><img src="{{ asset('images/grafit.jpg') }}" class="img-fluid"></div>
    <div class="col-12 col-lg-6 pt-5">
      <h1 class="mt-3 mb-3 text-sea_color">Hesaplı Kalite Keyfi</h1>
      <p>Caretta Banyo, 1971 yılında kurulmuş olup, alanında uzman kadrosu ve geniş ürün yelpazesiyle, siz değerli müşterilerimize hizmet vermektedir.</p>
      <p>Caretta Banyo, öncelikli olarak müşteri memnuniyetini hedefler, müşteri istekleri doğrultusunda hareket eder. Bu sebeple, satış öncesi ve sonrası destek konusunda en iyi hizmeti vermeye çalışıyor, bu konuda sürekli olarak gelişim gösteriyoruz. Bu alanda gelişme ve yenilikleri takip ederken, teknik bakımdan kendimizi güçlendiriyoruz.</p>
      <p>Caretta Banyo, ile hayallerinizdeki banyoya sahip olabilirsiniz. Biz bunun için sizlere en kaliteli hizmeti sunmaya devam edeceğiz...</p>
      <p>Uzman ekibimiz ve danışmanlarımızla yerinde ölçüm, inceleme ve en güzel önerilerimizle ürününüzün Caretta Banyo güvencesi ile adresinize teslim ediyoruz.</p>
    </div>
  </div>
  <div class="row mt-5">
    <div class="col">
      <div class="haberler owl-carousel">
        @foreach($haberler as $haber)
        <div>
          <a data-fancybox data-type="iframe" data-src="{{ url('/news',['id'=>$haber->id,'baslik'=>str_slug($haber->baslik)]) }}" href="javascript:;">
            <div class="haber_gorsel"><img src="{{ asset('images/news/'.$haber->haber_gorsel) }}" class="img-flud"></div>
            <div>
              <p><strong>{{ $haber->baslik }}</strong></p>
              <p>{!! str_limit($haber->icerik,60) !!}</p>
            </div>
          </a>
        </div>
        @endforeach
        <div>
          <div><img src="{{ asset('images/katalog.jpg') }}" class="img-flud"></div>
          <div>
            <p><strong>Katalog &amp; Broşür</strong></p>
            <p>2018 Ürün Kataloğumuzu İndirmek için tıklayınız.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-5 mb-5">
    <div class="col-12 col-sm-6 col-lg-4 mb-3">
      <img src="{{ asset('images/mini_gallery/image1.jpg')}}" class="img-fluid w-100">
    </div>
    <div class="col-12 col-sm-6 col-lg-4 mb-3">
      <div class="row">
        <div class="col-12">
          <img src="{{ asset('images/mini_gallery/image2.jpg')}}" class="img-fluid w-100">
        </div>
      </div>
      <div class="row gallery_top_margin">
        <div class="col-6">
          <img src="{{ asset('images/mini_gallery/image3.jpg')}}" class="img-fluid w-100">
        </div>
        <div class="col-6">
          <img src="{{ asset('images/mini_gallery/image3.jpg')}}" class="img-fluid w-100">
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-lg-4 mb-3">
      <img src="{{ asset('images/mini_gallery/image4.jpg')}}" class="img-fluid w-100">
    </div>
  </div>
</div>



@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {

    $(".owl-carousel").owlCarousel({
      items: 5,
      margin:30,
      responsive: {
        0: {
          items: 2,
          margin:20
        },
        480: {
          items: 3
        },
        992: {
          items: 5,
          mouseDrag: false,
          touchDrag: false
        }
      }
    });


    var slider = new MasterSlider();
			slider.setup( 'masterslider', {
				width: 1410,
				height: 471,
				space: 5,
				minHeight: 250,
				layout: "fullwidth",
				autoplay: true,
				centerControls:false,
				loop: true,
				view:'parallaxMask'
					// more slider options goes here...
					// check slider options section in documentation for more options.
			} );
			// adds Arrows navigation control to the slider.
			slider.control( 'arrows' );

      $current_page = window.location.href;

      $("[data-fancybox]").fancybox({
          iframe : {
            preload : true,
            css : {
                width : '1000px'
            }
          },
          afterLoad: function(current, previous) {
            window.history.pushState({page: "carettabanyo"}, "", $(this).attr('src'));
          },
          afterClose: function() {
            window.history.pushState({page: "carettabanyo"}, "", $current_page);
          }

      });
  })
</script>
@endsection
