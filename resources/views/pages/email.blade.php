<!doctype html>
		<html>
		<head>
		<meta charset='utf-8'>
		<title>Mail</title>

		<style>

			.container {
				font-family:Arial,Helvetica,sans-serif;
				border:5px solid #dfe4e9;
				border-radius:15px;
				padding:20px;
				font-size:13px;
				color:#6d767e;
				position:relative;
			}

			.logo {
				max-height:0;
				max-width:0;
				overflow:visible;
			}
			.baslik {
				font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif;
				font-weight: bold;
				color:#ed5565;
				font-size:18px;
				text-align:right;
				margin-top:10px;

			}


			table {
				width:100%;
				margin-top:20px;
				margin-bottom:30px;
			}
			table tr:nth-child(even) {
				background-color:#e6eaed;
			}

			table tr td:first-child {
				width:100px;
			}

			table tr td {
				padding:10px;
				vertical-align: top;
			}

			.altmesaj {
				font-family:Arial,Helvetica, sans-serif;
				font-size:12px;
				color:#6d767e;
				padding-left:10px;
				margin-top:5px;
			}

		</style>
		</head>
    <body>
			<div class='container'>
				<div class='baslik'>İletişim Formu</div>
				<table>
					<tr>
						<td>Adı Soyadı</td>
						<td>{{ $name }}</td>
					</tr>
					<tr style='background-color:#e6eaed;'>
						<td>E-posta</td>
						<td>{{ $email }}</td>
					</tr>
					<tr>
						<td>Telefon</td>
						<td>{{ $phone }}</td>
					</tr>
					<tr style='background-color:#e6eaed;'>
						<td>Konu</td>
						<td>{{ $subject }}</td>
					</tr>
					<tr>
						<td>Mesaj</td>
						<td>{{ $user_message }}</td>
					</tr>
				</table>
			</div>

			<div class='altmesaj'>Bu mesaj <cite style='color:#CC0000'>carettabanyo.com</cite> sitesinden gönderilmektedir.</div>
		</body>
		</html>
