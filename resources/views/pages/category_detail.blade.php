@extends('layouts.master')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col p-0">
      <div class="master-slider ms-skin-default" id="masterslider">

						<!-- new slide -->
						<div class="ms-slide">
							<img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="{{ asset('images/banner1.jpg') }}"/>
							<!-- slide image layer -->
              <!-- slide text layer -->
              <div class="ms-layer ms-caption"
                   data-offset-x      = "100"
                   data-offset-y      = "70"
                   data-position      = "normal"
                   data-origin        = "tl"
                   data-type          = "text"
                   data-effect        = "bottom(90)"
                   data-duration      = "800"
                   data-ease          = "easeOutQuart"
                   data-resize        = "false"
                   data-fixed         = "true"
                   data-widthlimit    = "600"
              >
               <h5>Hayallerinizi<br>gerçekleştiren banyolar</h5>
              </div>

              <!-- slide text layer -->
              <div class="ms-layer ms-caption"
                   data-offset-x      = "100"
                   data-offset-y      = "80"
                   data-position      = "normal"
                   data-origin        = "tr"
                   data-type          = "text"
                   data-effect        = "bottom(90)"
                   data-duration      = "800"
                   data-ease          = "easeOutQuart"
                   data-resize        = "false"
                   data-fixed         = "true"
                   data-widthlimit    = "600"
              >
               <h5>{{ $category->name }}</h5>
              </div>
						</div>
						<!-- end slide -->


					</div> <!-- master slider end -->
    </div>
  </div>
</div>

<div class="container mt-4">
  <div class="row">
    <div class="col-auto">
      <h3 class="mb-4 text-sea_color" style="font-weight:600;">Ürün Tipi Seçiniz</h3>
    </div>
    <div class="col text-right">
      @if(count($attributes)>0)
        <div class="form-check form-check-inline">
          <input class="form-check-input attribute_filter" type="radio" name="attributes[]" id="all_attributes" data-filter="*" checked>
          <label class="form-check-label" for="all_attributes">Tümü</label>
        </div>
        @foreach($attributes as $attribute)
        <div class="form-check form-check-inline">
          <input class="form-check-input attribute_filter" type="radio" name="attributes[]" id="{{ str_slug($attribute->name)}}" data-filter=".{{ str_slug($attribute->name)}}">
          <label class="form-check-label" for="{{ str_slug($attribute->name)}}">{{ $attribute->name }}</label>
        </div>
        @endforeach
      @endif
    </div>
  </div>
  <div class="row">
    <div class="category_holder owl-carousel">

      @foreach($category->subcategories as $subcategories)
      <div class="subcategories">
        <div><img data-filter=".{{ str_slug($subcategories->name) }}" src="{{ asset('images/categories/'.$subcategories->category_image) }}" class="img-fluid w-100"></div>
        <div class="text-center"><strong>{{ $subcategories->name }}</strong></div>
      </div>
      @endforeach
    </div>

  </div>
  <div class="row mt-4">
    <div class="col-12 text-center mt-2 mb-3"><h3 class="mb-4 text-sea_color" style="font-weight:600;">{{ $category->name }}</h3></div>

    <div class="grid w-100">

      @foreach($category->subcategories as $subcategories)
      <!-- Eğer Alt Kategori varsa -->
        @if(count($subcategories->subcategories)>0)

          @foreach($subcategories->subcategories as $sub_sub)
          <div class="{{ str_slug($subcategories->name) }}

            @if($sub_sub->products)
            @foreach($sub_sub->products as $products)
            @if($products->tags)
            @foreach($products->tags as $tags)
              {{ str_slug($tags->name) }}
            @endforeach
            @endif
            @endforeach
            @endif

             grid-item col-6 col-md-3 col-lg-2  mb-4">
            <a data-fancybox data-type="iframe" data-src="{{ url('/product',['id'=>$sub_sub->id,'type'=>'category','name'=>str_slug($sub_sub->name)]) }}" href="javascript:;">
              <div class="text-center mb-3">
                @if($sub_sub->category_image)
                  <img src="{{ asset('images/categories/'.$sub_sub->category_image) }}" class="img-fluid w-100">
                @else
                  <img src="{{ asset('images/bulk1.jpg') }}" class="img-fluid w-100">
                @endif
                </div>
              <div class="text-center text-koyu-gri mb-2"><strong class="name">{{$sub_sub->name}}</strong></div>
            </a>
          </div>
          @endforeach

        @else

          @foreach($subcategories->products as $product)
          <div class="{{ str_slug($subcategories->name) }}
            @if(count($product->attributes)>0)
              @foreach($product->attributes as $attribute)
                {{ str_slug($attribute->name) }}
              @endforeach
            @endif

            @if(count($product->tags)>0)
              @foreach($product->tags as $tag)
                {{ str_slug($tag->name) }}
              @endforeach
            @endif

          grid-item col-6 col-md-3 col-lg-2  mb-4">
            <a data-fancybox data-type="iframe" data-src="{{ url('/product',['id'=>$product->id,'type'=>'product','name'=>str_slug($product->name)]) }}" href="javascript:;">
              <div class="text-center mb-3"><img src="{{ asset('images/products/small/'.$product->product_image) }}" class="img-fluid w-100"></div>
              <div class="text-center text-koyu-gri mb-2"><strong class="name">{{$product->name}}</strong></div>
            </a>
          </div>
          @endforeach

        @endif

      @endforeach
    </div> <!--grid end -->
  </div>
</div>



@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {
    var $grid = $('.grid').isotope({
      getSortData: {
        name: '.name', // text from querySelector
      },
      sortBy: 'name'
    });

    $grid.imagesLoaded().progress( function() {
      $grid.isotope('layout');
    });

    $(".subcategories").on('click','img',function() {
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({ filter: filterValue });
      $(".subcategories img").removeClass('active');
      $(this).addClass('active');
      $('radio').each(function(index, value) {
        $(this).prop('checked',false);
      });
      $('#all_attributes').prop('checked','checked');
    })

    $('.attribute_filter').on('click',function() {
      var filterValue = $(this).attr('data-filter');
      $(".subcategories img").removeClass('active');
      $grid.isotope({ filter: filterValue });
    })

    $(".owl-carousel").owlCarousel({
      margin:30,
      nav: true,
      navText: [
         "<i style='font-size:30px;' class='fa fa-chevron-left'></i>",
         "<i style='font-size:30px;' class='fa fa-chevron-right'></i>"
      ],
      responsive: {
        0: {
          items: 2,
          margin:20
        },
        480: {
          items: 3
        },
        992: {
          items: 6,
        }
      }
    });

    var slider = new MasterSlider();
			slider.setup( 'masterslider', {
				width: 1920,
				height: 176,
				space: 5,
				minHeight: 176,
				layout: "fullwidth",
				autoplay: true,
				centerControls:false,
				loop: true,
				view:'parallaxMask'
					// more slider options goes here...
					// check slider options section in documentation for more options.
			} );
			// adds Arrows navigation control to the slider.
			slider.control( 'arrows' );

      $current_page = window.location.href;

      $("[data-fancybox]").fancybox({
          iframe : {
            preload : true,
            css : {
                width : '1000px'
            }
          },
          afterLoad: function(current, previous) {
            window.history.pushState({page: "carettabanyo"}, "", $(this).attr('src'));
          },
          afterClose: function() {
            window.history.pushState({page: "carettabanyo"}, "", $current_page);
          }

      });

  })
</script>
@endsection
