@extends('layouts.master')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col p-0">
      <div class="master-slider ms-skin-default" id="masterslider">

						<!-- new slide -->
						<div class="ms-slide">
							<img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="{{ asset('images/banner1.jpg') }}"/>
							<!-- slide image layer -->
              <!-- slide text layer -->
              <div class="ms-layer ms-caption"
                   data-offset-x      = "100"
                   data-offset-y      = "70"
                   data-position      = "normal"
                   data-origin        = "tl"
                   data-type          = "text"
                   data-effect        = "bottom(90)"
                   data-duration      = "800"
                   data-ease          = "easeOutQuart"
                   data-resize        = "false"
                   data-fixed         = "true"
                   data-widthlimit    = "600"
              >
               <h5>Hayallerinizi<br>gerçekleştiren banyolar</h5>
              </div>

              <!-- slide text layer -->
              <div class="ms-layer ms-caption"
                   data-offset-x      = "100"
                   data-offset-y      = "80"
                   data-position      = "normal"
                   data-origin        = "tr"
                   data-type          = "text"
                   data-effect        = "bottom(90)"
                   data-duration      = "800"
                   data-ease          = "easeOutQuart"
                   data-resize        = "false"
                   data-fixed         = "true"
                   data-widthlimit    = "600"
              >
               <h5>Arama Sonuçları</h5>
              </div>
						</div>
						<!-- end slide -->


					</div> <!-- master slider end -->
    </div>
  </div>
</div>

<div class="container mt-4">

  <div class="row mt-4">
    <div class="col-12 text-center mt-2 mb-3"><h3 class="mb-4 text-sea_color" style="font-weight:600;">Arama Sonuçları</h3></div>

    <div class="grid w-100">
      <table class="table table-sm">
        <tr>
          <th>Ürün Adı</th>
          <th>Kategorisi</th>
        </tr>
        @foreach($arama_sonuclari as $product)
        <tr>
          <td><a data-fancybox data-type="iframe" data-src="{{ url('/product',['id'=>$product->id,'type'=>'product','name'=>str_slug($product->name)]) }}" href="javascript:;">{{ $product->name }}</a></td>
          <td>
            @if($product->category)
              @if($product->category->parent)
                @if($product->category->parent->parent)
                  {{ $product->category->parent->parent->name }} >
                @endif
              {{ $product->category->parent->name." > ".$product->category->name }}
              @elseif($product->category)
              {{ $product->category->name }}
              @endif
            @endif
          </td>
        </tr>
        @endforeach
      </table>
    </div> <!--grid end -->
  </div>
</div>



@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {
    var $grid = $('.grid').isotope({
      getSortData: {
        name: '.name', // text from querySelector
      },
      sortBy: 'name'
    });

    $grid.imagesLoaded().progress( function() {
      $grid.isotope('layout');
    });

    $(".subcategories").on('click','img',function() {
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({ filter: filterValue });
      $(".subcategories img").removeClass('active');
      $(this).addClass('active');
      $('radio').each(function(index, value) {
        $(this).prop('checked',false);
      });
      $('#all_attributes').prop('checked','checked');
    })

    $('.attribute_filter').on('click',function() {
      var filterValue = $(this).attr('data-filter');
      $(".subcategories img").removeClass('active');
      $grid.isotope({ filter: filterValue });
    })

    $(".owl-carousel").owlCarousel({
      margin:30,
      nav: true,
      navText: [
         "<i style='font-size:30px;' class='fa fa-chevron-left'></i>",
         "<i style='font-size:30px;' class='fa fa-chevron-right'></i>"
      ],
      responsive: {
        0: {
          items: 2,
          margin:20
        },
        480: {
          items: 3
        },
        992: {
          items: 6,
        }
      }
    });

    var slider = new MasterSlider();
			slider.setup( 'masterslider', {
				width: 1920,
				height: 176,
				space: 5,
				minHeight: 176,
				layout: "fullwidth",
				autoplay: true,
				centerControls:false,
				loop: true,
				view:'parallaxMask'
					// more slider options goes here...
					// check slider options section in documentation for more options.
			} );
			// adds Arrows navigation control to the slider.
			slider.control( 'arrows' );

      $current_page = window.location.href;

      $("[data-fancybox]").fancybox({
          iframe : {
            preload : true,
            css : {
                width : '1000px'
            }
          },
          afterLoad: function(current, previous) {
            window.history.pushState({page: "carettabanyo"}, "", $(this).attr('src'));
          },
          afterClose: function() {
            window.history.pushState({page: "carettabanyo"}, "", $current_page);
          }

      });

  })
</script>
@endsection
