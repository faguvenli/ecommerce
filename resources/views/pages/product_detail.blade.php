@extends('layouts.iframe_main')

@section('content')

<div class="content">
  <div class="container-fluid">
    @if($main_product)
    <div class="row">
      <div class="col-12 col-lg-7">
        <img src="{{ asset('images/products/large/'.$main_product->product_image) }}" class="img-fluid w-100">
      </div>
      <div class="col-12 col-lg-5">
        <div class="row" style="height:100%">
          <div class="col-12">
            <div class="bg-acik-gri detail_column" style="height:100%">
              <p class="ana_kategori_adi">
                @if($main_product->category->parent->parent)
                  {{ $main_product->category->parent->parent->name }}
                @else
                  {{ $main_product->category->parent->name }}
                @endif
              </p>
              <h2 class="text-sea_color mt-1"><strong>{{ $main_product->category->name }}</strong></h2>
              <h3 class="mt-4 urun_adi">{{ $main_product->name }}</h3>
              <div class="mt-4">{!! $main_product->description !!}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if(count($related_products)>0)
    <div class="row">
      <div class="col-12 text-sea_color mb-2 mt-3"><h5>Tüm Ölçüler</h5></div>
      @foreach($related_products as $related_product)

        <div class="col-6 col-md-3 col-lg-2 mb-4">
            <div class="text-center">
              @if($related_product->product_image)
              <img src="{{ asset('images/products/medium/'.$related_product->product_image) }}" class="img-fluid w-100">
              @else
              <img src="{{ asset('images/bulk2.jpg') }}" class="img-fluid w-100">
              @endif
            </div>
            <div class="text-center iliskili_urun_adi bg-acik-gri mb-2">{{$related_product->name}}</div>
            <div>{!! $related_product->description !!}</div>
        </div>

      @endforeach
    </div>
    @endif
  </div>
  @endif
</div>


@endsection

@section('custom_scripts')
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
