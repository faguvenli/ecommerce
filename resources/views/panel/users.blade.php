@extends('layouts.panel_master')

@section('breadcrumb')

<!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item active">Kullanıcılar</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@endsection

@section('content')


  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 offset-md-10">

          <a href="{{route('register')}}" class="btn btn-primary btn-block mb-3">Ekle</a>

        </div>
        <!-- /.col -->
        <div class="col-12">

          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="users_table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>İsim</th>
                            <th style="width:60px; min-width:60px; max-width:60px;">İşlem</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)

                            <tr>
                                <td>{{ $user->name }}</td>
                                <td class="text-center">
                                  <a class="btn btn-sm btn-info mr-3" href="{{ route('users.edit', ['id'=>$user->id]) }}">Düzenle</a>
                                </td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.content -->

    <script>
        $(document).ready(function() {
          $("#users_table").DataTable();
        })
    </script>

@endsection
