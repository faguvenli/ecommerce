@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/panel">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('news.index')}}">Haberler</a></li>
            <li class="breadcrumb-item active">Haber Ekle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::open(['route'=>'news.store', 'files'=>'true', 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Haber Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          {{ Form::label('baslik', 'Başlık', ['class' => 'col-form-label']) }}
                          <div class="input-group mb-3 col-12">
                            {{ Form::text('baslik', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Başlık zorunlu alan.']) }}
                            <div class="input-group-append">
                                <span class="fas fa-vector-square fa-fw input-group-text"></span>
                            </div>
                          </div>
                        </div>


                        <div class="form-row">
                          {{ Form::label('icerik', 'Açıklama', ['class' => 'col-form-label']) }}
                          <div class="col-12 mb-3">
                          {{ Form::text('icerik', null, ['class'=>'form-control tinymce']) }}
                          </div>
                        </div>


                        <div class="form-row">
                          {{ Form::label('haber_gorsel','Haber Görseli Seçin', ['class'=>'col-form-label']) }}
                          <div class="col-12">
                            <div class="input-group">
                              <div class="custom-file">
                                {{ Form::file('haber_gorsel',['class'=>'custom-file-input'])}}
                                {{ Form::label('haber_gorsel','HAber Görseli Seçin', ['class'=>'custom-file-label']) }}
                              </div>
                              <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
