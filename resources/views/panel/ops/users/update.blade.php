@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Kullanıcılar</a></li>
            <li class="breadcrumb-item active">Kullanıcı Düzenle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')




  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::model($user, ["route"=>["users.update",$user->id],"method"=>"put","data-parsley-validate"=>"", "data-parsley-errors-container"=>".parsley_error"]) !!}


                <div class="form-row">
                  <label class="col-3 col-md-2 col-form-label">Ad Soyad</label>
                  <div class="input-group mb-3 col-9 col-md-10">
                    <input type="text" id="name" name="name" value="{{ $user->name }}" class="form-control" required autofocus data-parsley-error-message="Ad Soyad zorunlu alan">
                    <div class="input-group-append">
                        <span class="fas fa-id-card fa-fw input-group-text"></span>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <label class="col-3 col-md-2 col-form-label">Eposta</label>
                  <div class="input-group mb-3 col-9 col-md-10">
                    <input type="email" id="email" name="email" value="{{ $user->email }}" class="form-control" required data-parsley-error-message="Eposta zorunlu alan">
                    <div class="input-group-append">
                        <span class="fas fa-at fa-fw input-group-text"></span>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <label class="col-3 col-md-2 col-form-label">Parola</label>
                  <div class="input-group mb-3 col-9 col-md-10">
                    <input type="password" id="password" name="password" class="form-control">
                    <div class="input-group-append">
                        <span class="fas fa-lock fa-fw input-group-text"></span>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <label class="col-3 col-md-2 col-form-label">Parola (Tekrar)</label>
                  <div class="input-group mb-3 col-9 col-md-10">
                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                    <div class="input-group-append">
                        <span class="fas fa-lock fa-fw input-group-text"></span>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->

                  <div class="col-2">
                    <input type="submit" class="btn btn-primary btn-block" value="Güncelle">
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}


              <div class="row mt-3">
                <div class="col-10"></div>
                <div class="col-2">
                  {!! Form::open(['url' => '/users/'.$user->id, 'method' => 'delete', 'class' => 'text-right']) !!}
                  {!! Form::submit('Sil', ['class' => 'btn btn-danger btn-block sil_btn']) !!}
                  {!! Form::close() !!}
                </div>
              </div>
            

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
