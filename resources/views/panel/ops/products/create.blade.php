@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('products.index')}}">Ürünler</a></li>
            <li class="breadcrumb-item active">Ürün Ekle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::open(['route'=>'products.store', 'files'=>'true', 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Ürün Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          <div class="col-12 col-md-4">
                            {{ Form::label('name', 'Adı', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('name', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('category_id', 'Kategorisi', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              <select name="category_id" class="form-control selectpicker" multiple data-max-options="1">
                                @foreach($categories as $category)
                                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                                  @foreach($category->subcategories as $subcategory)
                                    <option value="{{ $subcategory->id }}">{{ $category->name." > ".$subcategory->name }}</option>
                                    @foreach($subcategory->subcategories as $sub_sub)
                                      <option value="{{ $sub_sub->id }}">{{ $category->name." > ".$subcategory->name." > ".$sub_sub->name }}</option>
                                    @endforeach
                                  @endforeach
                                @endforeach
                              </select>
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('related_product_id', 'İlişkili Ürün', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('related_product_id',$related_products->pluck('name','id') , null, ['class'=>'form-control selectpicker','multiple'=>'','data-max-options'=>'1'])}}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 mt-3">
                            <table class="table table-sm table-bordered">
                              <thead>
                                <tr>
                                  <th>Özellikler</th>
                                  <th style="width:45px; min-width:45px; max-width:45px;"><button class="btn btn-sm btn-primary add_attribute"><i class="fas fa-plus-circle"></i></button></th>
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                            </table>
                          </div>

                          <div class="col-12 mb-3">
                            {{ Form::label('tag_id', 'Etiket', ['class' => 'col-form-label']) }}
                            {{ Form::select('tag_id[]',[] , null, ['class'=>'form-control tags','multiple'=>''])}}
                          </div>

                          <div class="col-12">
                            {{ Form::label('description', 'Açıklama', ['class' => 'col-form-label']) }}
                            <div class="mb-3">
                              {{ Form::text('description', null, ['class'=>'form-control tinymce'])}}
                            </div>
                          </div>

                          <div class="form-row">
                            {{ Form::label('product_image','Ürün Görseli Seçin', ['class'=>'col-form-label']) }}
                            <div class="col-12">
                              <div class="input-group">
                                <div class="custom-file">
                                  {{ Form::file('product_image',['class'=>'custom-file-input'])}}
                                  {{ Form::label('product_image','Ürün Görseli Seçin', ['class'=>'custom-file-label']) }}
                                </div>
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
