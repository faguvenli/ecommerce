@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('categories.index')}}">Kategoriler</a></li>
            <li class="breadcrumb-item active">Kategori Düzenle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::model($category,['route'=>['categories.update',$category->id],'method'=>'put', 'files'=>'true', 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Kategori Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          {{ Form::label('name', 'Adı', ['class' => 'col-form-label']) }}
                          <div class="input-group mb-3 col-12">
                            {{ Form::text('name', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                            <div class="input-group-append">
                                <span class="fas fa-vector-square fa-fw input-group-text"></span>
                            </div>
                          </div>
                        </div>

                        <div class="form-row">
                          {{ Form::label('parent_id', 'Üst Kategorisi', ['class' => 'col-form-label']) }}
                          <div class="input-group mb-3 col-12">
                            <select name="parent_id" class="form-control selectpicker">
                              @foreach($categories as $category_data)
                                <option value="{{ $category_data->id }}" {{ ($category_data->id == $category->parent_id)?'selected':null }}>{{ $category_data->name }}</option>
                                @foreach($category_data->subcategories as $subcategory)
                                  <option value="{{ $subcategory->id }}" {{ ($subcategory->id == $category->parent_id)?'selected':null }}>&nbsp;&nbsp;- {{ $subcategory->name }}</option>
                                @endforeach
                              @endforeach
                            </select>
                            <div class="input-group-append">
                                <span class="fas fa-vector-square fa-fw input-group-text"></span>
                            </div>
                          </div>
                        </div>

                        <div class="form-row">
                          {{ Form::label('description', 'Açıklama', ['class' => 'col-form-label']) }}
                          <div class="col-12 mb-3">
                          {{ Form::text('description', null, ['class'=>'form-control tinymce']) }}
                          </div>
                        </div>


                        <div class="form-row">
                          {{ Form::label('category_image','Kategori Görseli Seçin', ['class'=>'col-form-label']) }}
                          <div class="col-12">
                            <div class="input-group">
                              <div class="custom-file">
                                {{ Form::file('category_image',['class'=>'custom-file-input'])}}
                                {{ Form::label('category_image','Kategori Görseli Seçin', ['class'=>'custom-file-label']) }}
                              </div>
                              <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        @if($category->category_image)
                        <div class="form-row mt-3">
                          <div class="col-12">
                            <img style="width:50px;" src="{{ asset('images/categories/'.$category->category_image)}}">
                            <a href="{{url('/panel/delete_category_image',$category->id)}}">Görseli sil</a>
                          </div>
                        </div>
                        @endif

                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}
              <div class="row mt-3">
                <div class="col-10">

                </div>
                <div class="col-2">
                  {!! Form::open(['url' => '/panel/categories/'.$category->id, 'method' => 'delete', 'class' => 'text-right']) !!}
                  {!! Form::submit('Sil', ['class' => 'btn btn-danger btn-block sil_btn']) !!}
                  {!! Form::close() !!}
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
