@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('categories.index')}}">Kategoriler</a></li>
            <li class="breadcrumb-item active">Kategori Ekle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::open(['route'=>'categories.store', 'files'=>'true', 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Kategori Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          {{ Form::label('name', 'Adı', ['class' => 'col-form-label']) }}
                          <div class="input-group mb-3 col-12">
                            {{ Form::text('name', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                            <div class="input-group-append">
                                <span class="fas fa-vector-square fa-fw input-group-text"></span>
                            </div>
                          </div>
                        </div>

                        <div class="form-row">
                          {{ Form::label('parent_id', 'Üst Kategorisi', ['class' => 'col-form-label']) }}
                          <div class="input-group mb-3 col-12">
                            <select name="parent_id" class="form-control selectpicker">
                              @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @foreach($category->subcategories as $subcategory)
                                  <option value="{{ $subcategory->id }}">&nbsp;&nbsp;- {{ $subcategory->name }}</option>
                                @endforeach
                              @endforeach
                            </select>
                            <div class="input-group-append">
                                <span class="fas fa-vector-square fa-fw input-group-text"></span>
                            </div>
                          </div>
                        </div>

                        <div class="form-row">
                          {{ Form::label('description', 'Açıklama', ['class' => 'col-form-label']) }}
                          <div class="col-12 mb-3">
                          {{ Form::text('description', null, ['class'=>'form-control tinymce']) }}
                          </div>
                        </div>


                        <div class="form-row">
                          {{ Form::label('category_image','Kategori Görseli Seçin', ['class'=>'col-form-label']) }}
                          <div class="col-12">
                            <div class="input-group">
                              <div class="custom-file">
                                {{ Form::file('category_image',['class'=>'custom-file-input'])}}
                                {{ Form::label('category_image','Kategori Görseli Seçin', ['class'=>'custom-file-label']) }}
                              </div>
                              <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
