@extends('layouts.panel_master')

@section('breadcrumb')


  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item active">Ürünler</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->


@endsection

@section('content')


  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 offset-md-10 text-right">
          <a href="{{ route('products.create') }}" class="btn btn-primary mb-3">Ekle</a>
        </div>
        <!-- /.col -->
        <div class="col-12">

          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="data_table" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Adı</th>
                      <th>Kategorisi</th>
                      <th>İlişkili Ürün</th>
                      <th style="width:120px; min-width:120px; max-width:120px;">İşlemler</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($products as $product)
                    <tr>
                      <td>{{ $product->name }}</td>
                      <td>
                        @if($product->category)
                          @if($product->category->parent)
                            @if($product->category->parent->parent)
                              {{ $product->category->parent->parent->name }} >
                            @endif
                          {{ $product->category->parent->name." > ".$product->category->name }}
                          @elseif($product->category)
                          {{ $product->category->name }}
                          @endif
                        @endif
                      </td>
                      <td>
                        @if($product->related_products)
                          {{ $product->related_products->name }}
                        @endif
                      </td>
                      <td class="text-center">
                        <a class="btn btn-sm btn-info mr-2" href="{{ route('products.edit', ['id'=>$product->id]) }}">Düzenle</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->



  <script>
    $(document).ready(function() {
      $("#data_table").DataTable();
    })
  </script>
@endsection
