@extends('layouts.panel_master')

@section('breadcrumb')


  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item active">Özellikler</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->


@endsection

@section('content')


  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 offset-md-10 text-right">
          <a href="{{ route('attributes.create') }}" class="btn btn-primary mb-3">Ekle</a>
        </div>
        <!-- /.col -->
        <div class="col-12">

          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="data_table" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Adı</th>
                      <th style="width:120px; min-width:120px; max-width:120px;">İşlemler</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($attributes as $attribute)
                    <tr>
                      <td>{{ $attribute->name }}</td>
                      <td class="text-center">
                        <a class="btn btn-sm btn-info mr-2" href="{{ route('attributes.edit', ['id'=>$attribute->id]) }}">Düzenle</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->



  <script>
    $(document).ready(function() {
     $("#data_table").DataTable();
    })
  </script>
@endsection
