<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Caretta') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/pages/app.js') }}"></script>
    <!-- Styles -->
    <link href="{{ asset('css/pages/app.css') }}" rel="stylesheet">
    <style>
      body {
        margin-bottom:20px;
      }
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <div class="container-fluid pt-3">
    @include('partials._messages')
    @yield('content')
  </div>
</div>
<!-- ./wrapper -->
</body>
</html>
