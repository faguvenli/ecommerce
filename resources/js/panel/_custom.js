function standartFormat(state) {
  if(state) {
    if(state.text != undefined) {
      return $('<div>' + state.text + '</div>');
    }
  }
}

function vt_search($selector, $placeHolder = 'Ara...', $url, $multiple = false, $template = standartFormat, $tags) {
  $($selector).select2({
    tags: $tags,
    closeOnSelect: !$multiple,
    placeholder: $placeHolder,
    templateResult: $template,
    allowClear:true,
    ajax: {
      url: $url,
      dataType: 'json',
      data: function(params) {
        return {
          q: $.trim(params.term)
        };
      },
      processResults: function(data) {
        return {
          results: data
        };
      },
      cache: true
    }
  })
}

$(document).ready(function() {

  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.extend( true, $.fn.dataTable.defaults, {
      lengthChange: false,
      'dom': '<"dataTables_wrapper"f<t><"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>',
      pageLength:100,
      "language": { "url": "/assets/datatables/turkce.json"},
  } );

  var editor_config = {
    path_absolute : "/",
    selector: ".tinymce",
    height:'180px',
    textcolor_map: ['000000', 'Black',
                    '1198a9', 'Sea Green',
                    '333300', 'Dark olive',
                    '003300', 'Dark green',
                    '003366', 'Dark azure',
                    '000080', 'Navy Blue',
                    '333399', 'Indigo',
                    '333333', 'Very dark gray',
                    '800000', 'Maroon',
                    'FF6600', 'Orange',
                    '808000', 'Olive',
                    '008000', 'Green',
                    '008080', 'Teal',
                    '0000FF', 'Blue',
                    '666699', 'Grayish blue',
                    '808080', 'Gray',
                    'FF0000', 'Red',
                    'FF9900', 'Amber',
                    '99CC00', 'Yellow green',
                    '339966', 'Sea green',
                    '33CCCC', 'Turquoise',
                    '3366FF', 'Royal blue',
                    '800080', 'Purple',
                    '999999', 'Medium gray',
                    'FF00FF', 'Magenta',
                    'FFCC00', 'Gold',
                    'FFFF00', 'Yellow',
                    '00FF00', 'Lime',
                    '00FFFF', 'Aqua',
                    '00CCFF', 'Sky blue',
                    '993366', 'Brown',
                    'C0C0C0', 'Silver',
                    'FF99CC', 'Pink',
                    'FFCC99', 'Peach',
                    'FFFF99', 'Light yellow',
                    'CCFFCC', 'Pale green',
                    'CCFFFF', 'Pale cyan',
                    '99CCFF', 'Light sky blue',
                    'CC99FF', 'Plum',
                    'FFFFFF', 'White'],
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);

  $(".selectpicker").selectpicker({
    liveSearch:true,
    liveSearchPlaceholder:"Ara...",
    noneSelectedText: "Hiçbirşey seçilmedi",
    title:"Seçiniz",
    noneResultsText:"Aradığınız kelime bulunamadı"
  });

  $(".sil_btn").on("click",function(e) {
    e.preventDefault();

    var form = $(this).closest('form');

    $.confirm({
          title: 'Uyarı!',
          content: 'Emin misiniz?',
          buttons: {
            formSubmit: {
              text: 'Evet',
              btnClass: 'btn btn-success',
              action: function() {
                  form.submit();
              }
            },
            cancel: {
              text: 'Hayır'
            }

          }
      });
  });

  $(".add_attribute").on("click",function(e) {
    e.preventDefault();

    $("table>tbody").append(
    "<tr>"+
      "<td><select name='attributes[]' class='form-control form-control-sm attributes'></select></td>"+
      "<td><button class='btn btn-sm btn-danger remove_attribute'><i class='fas fa-times-circle'></i></button></td>"+
    "</tr>");
    vt_search(".attributes", "Özellik Seçin", "/attribute_search",false,standartFormat,false);
  });

  $(document).on("click",".remove_attribute",function(e) {
    e.preventDefault();
    $(this).closest("tr").remove();
  })

  vt_search(".attributes", "Özellik Seçin", "/attribute_search",false,standartFormat,false);

  vt_search(".tags", "Etiket Seçin", "/tag_search",true,standartFormat,true);

})
