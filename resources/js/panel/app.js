
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
require("admin-lte");
require('datatables.net-bs4');
require("bootstrap-select");
require("jquery-confirm");
require("select2");
require('parsleyjs');
require("./_custom");
