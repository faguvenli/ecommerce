
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
require('owl.carousel');
require('isotope-layout/dist/isotope.pkgd.js');
require('@fancyapps/fancybox');
require('imagesloaded');
require('parsleyjs');
require('./_custom');
