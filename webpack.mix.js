const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/panel/app.js', 'public/js/panel')
   .sass('resources/sass/panel/app.scss', 'public/css/panel');

mix.js('resources/js/pages/app.js', 'public/js/pages')
    .sass('resources/sass/pages/app.scss', 'public/css/pages');
