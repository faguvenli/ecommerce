<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PagesController@getHome');
Route::get('/product/{id}/{type}/{name}', 'PagesController@getProduct');
Route::get('/category/{id}/{name}', 'PagesController@getCategory');
Route::get('/contact', 'PagesController@getContact');
Route::post('/contact/send_mail', 'PagesController@send_mail')->name('contact.send_mail');
Route::get('/news/{id}/{baslik}', 'PagesController@getNews');
Route::post('/search_page', 'PagesController@searchResults')->name('search_page');



Route::middleware(['auth'])->group(function() {
  // Anasayfa
  Route::get('/panel','HomeController@index');

  // Kategoriler
  Route::resource('/panel/categories','CategoryController');
  Route::get('/panel/delete_category_image/{id}','CategoryController@delete_category_image');

  // Özellikler
  Route::resource('/panel/attributes','AttributeController');
  Route::get('/attribute_search','AttributeController@attribute_search');

  // Etiketler
  Route::resource('/panel/tags','TagController');
  Route::get('/tag_search','TagController@tag_search');

  // Ürünler
  Route::resource('/panel/products','ProductController');
  Route::get('/panel/delete_product_image/{id}','ProductController@delete_product_image');

  Route::resource('/panel/users','UserController');
  Route::get('/panel/profile','UserController@profile')->name('profile');
  Route::post('/panel/profile/update/{user}','UserController@profile_update')->name('profile.update');

  // Haberler
  Route::resource('/panel/news','NewsController');
  Route::get('/panel/delete_news_image/{id}','NewsController@delete_news_image');

});
