<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use Session;

class AttributeController extends Controller
{
    public function attribute_search(Request $request) {


      $search = $request->q;
      $searchCriteria = explode(" ",$search);

      $veriler = Attribute::select('name','id')->where(function($q) use ($searchCriteria) {
        foreach($searchCriteria as $value) {
          $q->where('name','LIKE',"%{$value}%");
        }
      })
      ->orderBy('name')
      ->limit('200')
      ->get();

      $formatted_tags = [];
      foreach($veriler as $veri) {
        $formatted_tags[] = ['id'=>$veri->id, 'text'=>$veri->name];
      }

      return \Response::json($formatted_tags);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $attributes = Attribute::all();
      return view('panel.attributes')->with('attributes', $attributes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.attributes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('name');

        Attribute::create($data);

        Session::flash('success', 'Kayıt işlemi başarılı.');
        return redirect()->route('attributes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $attribute = Attribute::find($id);

      return view("panel.ops.attributes.update")
      ->with('attribute',$attribute);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('name');

        $attribute = Attribute::find($id);
        $attribute->update($data);

        Session::flash('success', 'Düzenleme işlemi başarılı.');
        return redirect()->route('attributes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kayit = Attribute::find($id);

      $kayit->delete($kayit->id);

      // Redirect
      Session::flash('success', 'Silme işlemi başarılı!');
      return redirect()->route('attributes.index');
    }
}
