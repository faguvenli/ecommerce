<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Category;
use Storage;
use Image;
use Session;

class CategoryController extends Controller
{
    private $image_small_width = 210;
    private $image_medium_width = 400;
    private $image_small_path = 'images/categories/';
    private $image_medium_path = 'images/categories/medium/';

    public function delete_category_image($id) {

      $category = Category::find($id);

      if(file_exists($this->image_small_path.$category->category_image)) {
        File::delete(public_path($this->image_small_path.$category->category_image));
        File::delete(public_path($this->image_medium_path.$category->category_image));
      }

      $category->update(['category_image'=>'']);

      Session::flash('success', 'Kategori Görseli Başarıyla Silindi.');
      return redirect()->back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::where('parent_id',0)->get();
      return view('panel.categories')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id',0)->orderBy('name')->get();

        return view('panel.ops.categories.create')
        ->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('name','description');



        //validate
        $this->validate($request, array(
          'name' => 'required',
        ));

        if($request->parent_id != NULL) {
          $data['parent_id'] = $request->input('parent_id');
        } else {
          $data['parent_id'] = 0;
        }

        if($request->category_image) {
          $request->validate(['category_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

          $category_image_name = str_slug($request->name).time().'.'.request()->category_image->getClientOriginalExtension();

          $location_small = public_path($this->image_small_path.$category_image_name);
          $location_medium = public_path($this->image_medium_path.$category_image_name);
          Image::make($request->file('category_image'))->resize($this->image_small_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location_small);
          Image::make($request->file('category_image'))->resize($this->image_medium_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location_medium);
          $data['category_image'] = $category_image_name;
        }



        Category::create($data);

        Session::flash('success', 'Kayıt işlemi başarılı.');
        return redirect()->route('categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = Category::find($id);
      $categories = Category::where('parent_id',0)->orderBy('name')->get();

      return view("panel.ops.categories.update")
      ->with('category',$category)
      ->with('categories',$categories);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name','description');

      $validator = $this->validate($request,array(
        'name' => "required",
      ));

      if($request->parent_id != NULL) {
        $data['parent_id'] = $request->input('parent_id');
      } else {
        $data['parent_id'] = 0;
      }

      if($request->category_image) {
        $request->validate(['category_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->delete_category_image($id);

        $category_image_name = str_slug($request->name).time().'.'.request()->category_image->getClientOriginalExtension();

        $location_small = public_path($this->image_small_path.$category_image_name);
        $location_medium = public_path($this->image_medium_path.$category_image_name);
        Image::make($request->file('category_image'))->resize($this->image_small_width,null, function($constraint) { $constraint->aspectRatio(); })->save($location_small);
        Image::make($request->file('category_image'))->resize($this->image_medium_width,null, function($constraint) { $constraint->aspectRatio(); })->save($location_medium);
        $data['category_image'] = $category_image_name;
      }


      $category = Category::find($id);
      $category->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('categories.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kayit = Category::find($id);

      $this->delete_category_image($id);

      $kayit->delete($kayit->id);

      // Redirect
      Session::flash('success', 'Silme işlemi başarılı!');
      return redirect()->route('categories.index');
    }
}
