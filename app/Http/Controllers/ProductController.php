<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Product;
use App\Category;
use App\Attribute;
use App\Tag;
use Storage;
use Image;
use Session;

class ProductController extends Controller
{
  private $image_small_width = 210;
  private $image_medium_width = 690;
  private $image_large_width = 800;
  private $image_small_path = 'images/products/small/';
  private $image_medium_path = 'images/products/medium/';
  private $image_large_path = 'images/products/large/';

  public function delete_product_image($id) {

    $product = Product::find($id);

    if(file_exists($this->image_small_path.$product->product_image)) {
      File::delete(public_path($this->image_small_path.$product->product_image));
    }
    if(file_exists($this->image_medium_path.$product->product_image)) {
      File::delete(public_path($this->image_medium_path.$product->product_image));
    }
    if(file_exists($this->image_large_path.$product->product_image)) {
      File::delete(public_path($this->image_large_path.$product->product_image));
    }

    $product->update(['product_image'=>'']);

    Session::flash('success', 'Ürün Görseli Başarıyla Silindi.');
    return redirect()->back();
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('panel.products')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id',0)->get();
        $related_products = Product::whereNull('related_product_id')->get();
        $attributes = Attribute::all();
        return view('panel.ops.products.create')
        ->with('categories',$categories)
        ->with('related_products',$related_products)
        ->with('attributes',$attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $data = $request->only('name','description','category_id','related_product_id');

      //validate
      $this->validate($request, array(
        'name' => 'required',
      ));

      if($request->product_image) {
        $request->validate(['product_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $product_image_name = str_slug($request->name).time().'.'.request()->product_image->getClientOriginalExtension();

        $location_small = public_path($this->image_small_path.$product_image_name);
        $location_medium = public_path($this->image_medium_path.$product_image_name);
        $location_large = public_path($this->image_large_path.$product_image_name);

        Image::make($request->file('product_image'))->resize($this->image_small_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location_small);
        Image::make($request->file('product_image'))->resize($this->image_medium_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize();  })->save($location_medium);
        Image::make($request->file('product_image'))->resize($this->image_large_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize();  })->save($location_large);

        $data['product_image'] = $product_image_name;
      }

      $product = Product::create($data);


      $tags = [];
      if($request->has('tag_id')) {
        foreach($request->tag_id as $tag_id) {
          if(Tag::find($tag_id)) {
            $tags[] = $tag_id;
          } else {
            $t = new Tag();
            $t->name = $tag_id;
            $t->save();
            $tags[] = $t->id;
          }
        }
      }

      $product->tags()->sync($tags);
      $product->attributes()->sync($request->input('attributes'));

      Session::flash('success', 'Kayıt işlemi başarılı.');
      return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $product = Product::find($id);
      $categories = Category::where('parent_id',0)->get();
      $related_products = Product::whereNull('related_product_id')->where('id','!=',$product->id)->get();
      $attributes = Attribute::all();
      $tags = Tag::all();
      return view("panel.ops.products.update")
      ->with('product',$product)
      ->with('categories',$categories)
      ->with('related_products',$related_products)
      ->with('attributes',$attributes)
      ->with('tags',$tags);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name','description','category_id','related_product_id');

      //validate
      $this->validate($request, array(
        'name' => 'required',
      ));

      $request->has('category_id')?$data['category_id'] = $request->input('category_id'):$data['category_id'] = null;
      $request->has('related_product_id')?$data['related_product_id'] = $request->input('related_product_id'):$data['related_product_id'] = null;

      if($request->product_image) {
        $request->validate(['product_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->delete_product_image($id);

        $product_image_name = str_slug($request->name).time().'.'.request()->product_image->getClientOriginalExtension();

        $location_small = public_path($this->image_small_path.$product_image_name);
        $location_medium = public_path($this->image_medium_path.$product_image_name);
        $location_large = public_path($this->image_large_path.$product_image_name);

        Image::make($request->file('product_image'))->resize($this->image_small_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize();  })->save($location_small);
        Image::make($request->file('product_image'))->resize($this->image_medium_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize();  })->save($location_medium);
        Image::make($request->file('product_image'))->resize($this->image_large_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize();  })->save($location_large);
        $data['product_image'] = $product_image_name;
      }

      $product = Product::find($id);
      $product->update($data);

      $tags = [];
      if($request->has('tag_id')) {
        foreach($request->tag_id as $tag_id) {
          if(Tag::find($tag_id)) {
            $tags[] = $tag_id;
          } else {
            $t = new Tag();
            $t->name = $tag_id;
            $t->save();
            $tags[] = $t->id;
          }
        }
      }

      $product->tags()->sync($tags);

      $product->attributes()->sync($request->input('attributes'));

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kayit = Product::find($id);

      $kayit->attributes()->detach();
      $kayit->tags()->detach();
      $kayit->delete($kayit->id);

      // Redirect
      Session::flash('success', 'Silme işlemi başarılı!');
      return redirect()->route('products.index');
    }
}
