<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Attribute;
use Mail;
use App\News;

class PagesController extends Controller
{
  public function getHome() {
    $haberler = News::select('*')->take('3')->orderBy('id','desc')->get();
    $main_menu = Category::where('parent_id',0)->with('subcategories')->get();
      return view('pages.home')->with('main_menu',$main_menu)->with('haberler',$haberler);
  }

  public function getCategory($id) {
    $main_menu = Category::where('parent_id',0)->with('subcategories')->orderBy('name')->get();
    $category = Category::find($id);

    $attributes = Attribute::whereHas('products',function($query) use ($id) {
      $query->whereHas('category', function($query) use ($id) {
        $query->where('parent_id',$id);
      });
    })->get();

      return view('pages.category_detail')
      ->with('main_menu',$main_menu)
      ->with('category',$category)
      ->with('attributes',$attributes);
  }

  public function getProduct($id,$type) {
    if($type == 'category') {
      $related_products = Product::where('category_id',$id)->whereNotNull('related_product_id')->get();
      $main_product = Product::where('category_id',$id)->whereNull('related_product_id')->first();
    }
    elseif($type == 'product') {
      $related_products = [];
      $main_product = Product::find($id);
    }

    return view('pages.product_detail')
    ->with('related_products',$related_products)
    ->with('main_product',$main_product);

  }

  public function getContact() {
    $main_menu = Category::where('parent_id',0)->with('subcategories')->get();
      return view('pages.contact')->with('main_menu',$main_menu);
  }

  public function send_mail(Request $request) {
    $data = array(
      'name' => $request->get('name'),
      'email' => $request->get('email'),
      'phone' => $request->get('phone'),
      'subject' => $request->get('subject'),
      'user_message' => $request->get('user_message')
    );

    Mail::send('pages.email', $data , function($message) {
      $message->from('otelaltay@gmail.com', 'Fevzi Altay Güvenli');
      $message->to('fevzialtay@gmail.com')->subject('Siteden Mesaj var.');
    });
    return back()->with('success', 'Mesajınız gönderildi.');

    //echo $request->input('user_message');
  }

  public function getNews($id,$baslik) {
    $haber = News::find($id);
      return view('pages.news_detail')->with('haber',$haber);
  }

  public function searchResults(Request $request) {
    $a = Product::where('name','like','%'.$request->input('arama').'%')->orWhere('description','like','%'.$request->input('arama').'%');

    $arama_sonuclari = $a->get();
    $main_menu = Category::where('parent_id',0)->with('subcategories')->get();
    return view('pages.search_results')->with('arama_sonuclari',$arama_sonuclari)->with('main_menu',$main_menu);
  }
}
