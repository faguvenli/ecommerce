<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Session;

class TagController extends Controller
{
    public function tag_search(Request $request) {


      $search = $request->q;
      $searchCriteria = explode(" ",$search);

      $veriler = Tag::select('name','id')->where(function($q) use ($searchCriteria) {
        foreach($searchCriteria as $value) {
          $q->where('name','LIKE',"%{$value}%");
        }
      })
      ->orderBy('name')
      ->limit('200')
      ->get();

      $formatted_tags = [];
      foreach($veriler as $veri) {
        $formatted_tags[] = ['id'=>$veri->id, 'text'=>$veri->name];
      }

      return \Response::json($formatted_tags);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tags = Tag::all();
      return view('panel.tags')->with('tags', $tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('name');

        Tag::create($data);

        Session::flash('success', 'Kayıt işlemi başarılı.');
        return redirect()->route('tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $tag = Tag::find($id);

      return view("panel.ops.tags.update")
      ->with('tag',$tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('name');

        $tag = Tag::find($id);
        $tag->update($data);

        Session::flash('success', 'Düzenleme işlemi başarılı.');
        return redirect()->route('tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kayit = Tag::find($id);

      $kayit->delete($kayit->id);

      // Redirect
      Session::flash('success', 'Silme işlemi başarılı!');
      return redirect()->route('tags.index');
    }
}
