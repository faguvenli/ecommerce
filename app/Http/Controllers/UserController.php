<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\User;
use Image;
use Session;

class UserController extends Controller
{

  public function profile() {
    $user = Auth::user();
     return view('panel.ops.users.profile')->with('user',$user);
  }

  public function profile_update(Request $request) {

    $id = Auth::id();
     if($request->password) {
          // Validate
          $this->validate($request, array(
            'name' => 'required|string|max:255',
            'email' => "required|string|email|max:255|unique:users,email,$id",
            'password' => 'required|string|min:6|confirmed',
          ));
        } else {
          // Validate
          $this->validate($request, array(
            'name' => 'required|string|max:255',
            'email' => "required|string|email|max:255|unique:users,email,$id"
          ));
        }



        $user = Auth::user();

          if($request->avatar) {
            $request->validate(['avatar'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

            $avatar_name = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

            $location = public_path('images/'.$avatar_name);
            Image::make($request->file('avatar'))->resize(100,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location);
            $user->avatar = $avatar_name;
          }

          $user->name = $request->input('name');
          $user->email = $request->input('email');

          if($request->password) {
            $user->password = bcrypt($request->input('password'));
          }

          $user->save();

          // Redirect
          Session::flash('success', 'Kayıt Güncellendi');

          $user = Auth::user();
          return redirect()->route('profile')->with('user',$user);

  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function index()
    {
        $users = User::all();

        return view('panel.users')->withUsers($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = User::find($id);


      return view('panel.ops.users.update')->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          if($request->password) {
            // Validate
            $this->validate($request, array(
              'name' => 'required|string|max:255',
              'email' => "required|string|email|max:255|unique:users,email,$id",
              'password' => 'required|string|min:6|confirmed',
            ));
          } else {
            // Validate
            $this->validate($request, array(
              'name' => 'required|string|max:255',
              'email' => "required|string|email|max:255|unique:users,email,$id"
            ));
          }

          $user = User::find($id);

          $user->name = $request->input('name');
          $user->email = $request->input('email');

          if($request->password) {
            $user->password = bcrypt($request->input('password'));
          }
          $user->save();

          // Redirect
          Session::flash('success', 'Kayıt Güncellendi');

          $users = User::all();
          return redirect()->route('users.index')->withUsers($users);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request,$id)
     {

      $kayit = User::find($id);

      $kayit->delete($kayit->id);

      // Redirect
      $users = User::all();
      return redirect()->route('users.index')->withUsers($users);
    }

}
