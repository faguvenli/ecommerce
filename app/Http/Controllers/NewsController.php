<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\News;
use Storage;
use Image;
use Session;

class NewsController extends Controller
{
  private $image_width = 800;
  private $image_path = 'images/news/';

  public function delete_news_image($id) {

    $news_detail = News::find($id);

    if(file_exists($this->image_path.$news_detail->haber_gorsel)) {
      File::delete(public_path($this->image_path.$news_detail->haber_gorsel));
    }

    $news_detail->update(['haber_gorsel'=>'']);

    Session::flash('success', 'Haber Görseli Başarıyla Silindi.');
    return redirect()->back();
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $news = News::all();
      return view('panel.news')->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('panel.ops.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->only('baslik','icerik');



      //validate
      $this->validate($request, array(
        'baslik' => 'required',
      ));


      if($request->haber_gorsel) {
        $request->validate(['haber_gorsel'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $haber_gorsel_name = str_slug($request->baslik).time().'.'.request()->haber_gorsel->getClientOriginalExtension();

        $location = public_path($this->image_path.$haber_gorsel_name);
        Image::make($request->file('haber_gorsel'))->resize($this->image_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location);
        $data['haber_gorsel'] = $haber_gorsel_name;
      }



      News::create($data);

      Session::flash('success', 'Kayıt işlemi başarılı.');
      return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $news_detail = News::find($id);

      return view("panel.ops.news.update")
      ->with('news_detail',$news_detail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('baslik','icerik');

      $validator = $this->validate($request,array(
        'baslik' => "required",
      ));

      if($request->haber_gorsel) {
        $request->validate(['haber_gorsel'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->delete_news_image($id);

        $haber_gorsel_name = str_slug($request->baslik).time().'.'.request()->haber_gorsel->getClientOriginalExtension();

        $location = public_path($this->image_path.$haber_gorsel_name);
        Image::make($request->file('haber_gorsel'))->resize($this->image_width,null, function($constraint) { $constraint->aspectRatio(); })->save($location);
        $data['haber_gorsel'] = $haber_gorsel_name;
      }


      $news_detail = News::find($id);
      $news_detail->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kayit = News::find($id);

      $this->delete_news_image($id);

      $kayit->delete($kayit->id);

      // Redirect
      Session::flash('success', 'Silme işlemi başarılı!');
      return redirect()->route('news.index');
    }
}
