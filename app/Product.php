<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    public function category() {
      return $this->belongsTo('App\Category');
    }

    public function related_products() {
      return $this->belongsTo('App\Product','related_product_id');
    }

    public function attributes() {
      return $this->belongsToMany('App\Attribute');
    }

    public function tags() {
      return $this->belongsToMany('App\Tag');
    }

}
